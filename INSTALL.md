# Install

1. To install **ACEofBASEs** stand-alone it is necessary first to have Python installed in
your system. Either you can install Python from its
[main site](https://www.python.org/) or use other distributions like
[Anaconda](https://www.anaconda.com/distribution/).

2. Next, install [Bowtie 1](http://bowtie-bio.sourceforge.net/index.shtml).
Follow the instructions in this web site, either simply download the latest
release or use [bioconda](http://bioconda.github.io/recipes/bowtie/README.html).

3. Finally, using `pip`, the Python installer, install **ACEofBASEs** with this command:

    ```
    $ pip install aceofbases
    ```
	
    This will download the software directly from the PyPI, the Python Package
Index. Alternatively, you can also install **ACEofBASEs** from the Git repository:
    
	```
    $ pip install https://bitbucket.org/juanlmateo/aceofbases_standalone/get/master.zip
    ```

