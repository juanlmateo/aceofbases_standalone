'''
Created on Jul 25, 2018

@author: juan
'''
import unittest

from aceofbases.base_editor import factory


class Test(unittest.TestCase):


    def test_score(self):
        base_editor = factory("BE4-Gam")

        self.assertEqual(base_editor.score_target("GGCCACCGGAGGCCGTCGCGGGG"),3)
        self.assertEqual(base_editor.score_target("GGGCTTTGTTCCTGTTCGTAAGG"),1)
        self.assertEqual(base_editor.score_target("CTTTGTTCCTGTTCGTAAGGAGG"),5)
        self.assertEqual(base_editor.score_target("AGGCCACCGGAGGCCGTCGCGGG"),3)
        self.assertEqual(base_editor.score_target("TGGATGACACACTGTCCGGAGGG"),2)
        self.assertEqual(base_editor.score_target("TACCTGTCCTCCTTACGAACAGG"),5)
        self.assertEqual(base_editor.score_target("CGGGGAACTTCGGAGAGCTACGG"),2)
        self.assertEqual(base_editor.score_target("GTGGATGACACACTGTCCGGAGG"),-2)
        self.assertEqual(base_editor.score_target("GAGCAGCCTGAGGCCACCGGAGG"),3)


if __name__ == "__main__":
    unittest.main()
