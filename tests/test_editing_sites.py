'''
Created on Jul 25, 2018

@author: juan
'''
import unittest

from aceofbases.editing_sites import EditingSites, EditingSite
from aceofbases.off_target import OffTarget
from aceofbases.bed_interval import BedInterval

class Test(unittest.TestCase):

    def test_scale_score_and_relabel(self):
        sites = EditingSites()
        sites.sites['C1'] = EditingSite( 'targetSeqC1', 'sequenceC1', 0, 'strand', 'fwdPrimer', 'revPrimer')
        sites.sites['C1'].label = 'C1'
        sites.sites['C1'].score = 406
        sites.sites['C2'] = EditingSite( 'targetSeqC2', 'sequenceC2', 0, 'strand', 'fwdPrimer', 'revPrimer')
        sites.sites['C2'].label = 'C2'
        sites.sites['C2'].score = 248
        sites.sites['C3'] = EditingSite( 'targetSeqC3', 'sequenceC3', 0, 'strand', 'fwdPrimer', 'revPrimer')
        sites.sites['C3'].label = 'C3'
        sites.sites['C3'].score = 724
        
        sites.scale_score_and_relabel()
        self.assertEqual(sites.get_site('T1').label, 'T1')
        self.assertEqual(sites.get_site('T1').sequence, 'sequenceC3')
        self.assertEqual(sites.get_site('T1').score, 724)
        self.assertEqual(sites.get_site('T2').label, 'T2')
        self.assertAlmostEqual(sites.get_site('T2').score, 406)
        self.assertEqual(sites.get_site('T2').sequence, 'sequenceC1')
        self.assertEqual(sites.get_site('T3').label, 'T3')
        self.assertEqual(sites.get_site('T3').score, 248)
        self.assertEqual(sites.get_site('T3').sequence, 'sequenceC2')

    def test_sort_offtargets(self):
        exons = BedInterval()
        exons.insert( 'chr1', 100, 150, 'gene_id', 'gene_name')
        exons.insert( 'chr2', 200, 250, 'gene_id', 'gene_name')
        exons.insert( 'chr3', 300, 350, 'gene_id', 'gene_name')
        genes = exons
        
        sites = EditingSites()
        sites.sites['C1'] = EditingSite( 'targetSeqC1', 'sequenceC1', 0, 'strand', 'fwdPrimer', 'revPrimer')
        sites.sites['C1'].label = 'C1'
        ot1 = OffTarget(False, 'chr2', '+', 400, '3:T>A', 'CCNAAAAAAAAAAAAAAAAAAAA', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        ot1 = OffTarget(False, 'chr3', '+', 50, '4:G>C', 'CCNCCCCCCCCCCCCCCCCCCCC', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        ot1 = OffTarget(False, 'chr2', '+', 1480, '3:C>G', 'CCNGGGGGGGGGGGGGGGGGGGG', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        
        sites.sort_offtargets()
        self.assertLess(sites.sites['C1'].off_targets[0].score, sites.sites['C1'].off_targets[1].score)
        self.assertEqual(sites.sites['C1'].off_targets[1].score, sites.sites['C1'].off_targets[2].score)
        self.assertLess(sites.sites['C1'].off_targets[1].distance, sites.sites['C1'].off_targets[2].distance)
        
        self.assertEqual(sites.sites['C1'].off_targets[0].sequence, 'GGGGGGGGGGGGGGGGGGCGNGG')
        self.assertEqual(sites.sites['C1'].off_targets[1].sequence, 'TTTTTTTTTTTTTTTTTTTANGG')
        self.assertEqual(sites.sites['C1'].off_targets[2].sequence, 'CCCCCCCCCCCCCCCCCCCGNGG')
        
        #Now with gene info, but some with no hits (comparing same score)
        exons = BedInterval()
        exons.insert( 'chr4', 100, 150, 'gene_id', 'gene_name')
        exons.insert( 'chr4', 200, 250, 'gene_id', 'gene_name')
        exons.insert( 'chr4', 300, 350, 'gene_id', 'gene_name')
        genes = exons
        sites = EditingSites()
        sites.sites['C1'] = EditingSite( 'targetSeqC1', 'sequenceC1', 0, 'strand', 'fwdPrimer', 'revPrimer')
        sites.sites['C1'].label = 'C1'
        ot1 = OffTarget(False, 'chr4', '+', 400, '3:T>A', 'CCNAAAAAAAAAAAAAAAAAAAA', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        ot1 = OffTarget(False, 'chr3', '+', 50, '4:G>C', 'CCNCCCCCCCCCCCCCCCCCCCC', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        ot1 = OffTarget(False, 'chr2', '+', 1480, '3:C>G', 'CCNGGGGGGGGGGGGGGGGGGGG', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        
        sites.sort_offtargets()
        self.assertLess(sites.sites['C1'].off_targets[0].score, sites.sites['C1'].off_targets[1].score)
        self.assertEqual(sites.sites['C1'].off_targets[1].score, sites.sites['C1'].off_targets[2].score)
        
        self.assertEqual(sites.sites['C1'].off_targets[0].sequence, 'GGGGGGGGGGGGGGGGGGCGNGG')
        self.assertEqual(sites.sites['C1'].off_targets[1].sequence, 'CCCCCCCCCCCCCCCCCCCGNGG')
        self.assertEqual(sites.sites['C1'].off_targets[2].sequence, 'TTTTTTTTTTTTTTTTTTTANGG')
        
        #Now without gene info
        exons = BedInterval()
        genes = exons
        sites = EditingSites()
        sites.sites['C1'] = EditingSite( 'targetSeqC1', 'sequenceC1', 0, 'strand', 'fwdPrimer', 'revPrimer')
        sites.sites['C1'].label = 'C1'
        ot1 = OffTarget(False, 'chr2', '+', 400, '3:T>A', 'CCNAAAAAAAAAAAAAAAAAAAA', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        ot1 = OffTarget(False, 'chr3', '+', 50, '4:G>C', 'CCNCCCCCCCCCCCCCCCCCCCC', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        ot1 = OffTarget(False, 'chr2', '+', 1480, '3:C>G', 'CCNGGGGGGGGGGGGGGGGGGGG', 20, 3, 12)
        sites.sites['C1'].add_offtarget(ot1,exons,genes)
        
        sites.sort_offtargets()
        self.assertLess(sites.sites['C1'].off_targets[0].score, sites.sites['C1'].off_targets[1].score)
        self.assertEqual(sites.sites['C1'].off_targets[1].score, sites.sites['C1'].off_targets[2].score)
        self.assertEqual(sites.sites['C1'].off_targets[1].distance, sites.sites['C1'].off_targets[2].distance)
        
        self.assertEqual(sites.sites['C1'].off_targets[0].sequence, 'GGGGGGGGGGGGGGGGGGCGNGG')
        self.assertEqual(sites.sites['C1'].off_targets[1].sequence, 'TTTTTTTTTTTTTTTTTTTANGG')
        self.assertEqual(sites.sites['C1'].off_targets[2].sequence, 'CCCCCCCCCCCCCCCCCCCGNGG')

if __name__ == "__main__":
    unittest.main()
