#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 12:02:26 2021

@author: juan
"""

import unittest

from aceofbases.main import get_html_translations
from aceofbases.main import get_formatted_translation
from aceofbases.main import is_within_exons

from aceofbases.sequence_methods import EditingWindowTranslation, CDS
from aceofbases.editing_sites import EditingSites
from aceofbases.base_editor import factory


class TestMain(unittest.TestCase):
    
    def test_get_html_translations(self):
        base_editor = factory("BE4-Gam")
        candidates = EditingSites()
        candidates.add("ACGTACGTACGTACGTACGT", "ACGTACGTACGTACGTACGTGGG", 10, "+", "fwdP", "revP", base_editor)
        cds = CDS("17", 1,240,"-", "ATGTTGAGGAACATGTTTGAGGAGTGGACTGAAATTCTACCACAAAAAAACAACGACAAGAGGTTCCTAACCGAACCAAACTTCACATTCAAATGGTATCATTCTCTGACGACTTTCTTTGCTTTGTTTTGTTTTTTTTTTCCTTTCACCAGAATCAACATGTATCTGGAGAACAAAAGCAACCTGGAGATGGAGATGACTCAGACTTCTGCCTCAGGTCATCAGTGCAGGAGCAGCCTGAGGCCACCGGAGGCCGTCGCGGGGAACTTCGGAGAGCTACGGCTGCTGCAGGGCATCTCAGAGAATGGAGACACCCAGAAGCTCCAGCAGTCGTCCGTCCTTCCCTCCGGACAGTGTGTCATCCACACAGAGGGCTTTGTTCCTGTTCGTAAGGAGGACAGACACAGCTTCAGCACTGTTAACTTCCTCAGAGGAAAAACAAATTCTGCAAACATTTCAGAGCGATCACCTCTGCTGAGA")
        cds.add_exon(1,401)
        cds.add_exon(3810,3888)

        html_translations = get_html_translations(candidates.get_sites_sorted(), cds, base_editor)
        self.assertTrue("MLRNMFEEWTEILPQKNNDKRFLTEPNFTFKWYHSLTTFFALFCFFFPFTRINMYLENKS" in html_translations)

    def test_get_formatted_translation(self):
        translation = EditingWindowTranslation(['P','E','L','G>K','D>N','S>N','S','Q','H'],3,6,9,16)
        html = get_formatted_translation(translation)
        self.assertTrue(">G>K<" in html)
        self.assertTrue(">D>N<" in html)
        self.assertTrue(">S>N<" in html)

    def test_is_within_exons(self):
        cds = CDS("17", 1,240,"-", "ATGTTGAGGAACATGTTTGAGGAGTGGACTGAAATTCTACCACAAAAAAACAACGACAAGAGGTTCCTAACCGAACCAAACTTCACATTCAAATGGTATCATTCTCTGACGACTTTCTTTGCTTTGTTTTGTTTTTTTTTTCCTTTCACCAGAATCAACATGTATCTGGAGAACAAAAGCAACCTGGAGATGGAGATGACTCAGACTTCTGCCTCAGGTCATCAGTGCAGGAGCAGCCTGAGGCCACCGGAGGCCGTCGCGGGGAACTTCGGAGAGCTACGGCTGCTGCAGGGCATCTCAGAGAATGGAGACACCCAGAAGCTCCAGCAGTCGTCCGTCCTTCCCTCCGGACAGTGTGTCATCCACACAGAGGGCTTTGTTCCTGTTCGTAAGGAGGACAGACACAGCTTCAGCACTGTTAACTTCCTCAGAGGAAAAACAAATTCTGCAAACATTTCAGAGCGATCACCTCTGCTGAGA")
        cds.add_exon(1,401)
        cds.add_exon(3810,3888)
        base_editor = factory("BE4-Gam")
        
        self.assertFalse(is_within_exons(3789,"-","TGTCTGAAACACAAAAATTG", cds, base_editor))
        self.assertTrue(is_within_exons(3789,"-","TGCCTGAAACACAAAAATTG", cds, base_editor))
        
        self.assertTrue(is_within_exons(381,"-","TACCTGTCCTCCTTACGAAC", cds, base_editor))
        self.assertTrue(is_within_exons(381,"-","TACNTGTCCTCCTTACGAAC", cds, base_editor))
        self.assertFalse(is_within_exons(381,"-","TACNTGTNNTCCTTACGAAC", cds, base_editor))
if __name__ == "__main__":
    unittest.main()
