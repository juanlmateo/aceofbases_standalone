#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 12:02:26 2021

@author: juan
"""

import unittest

from unittest.mock import patch

from aceofbases.sequence_methods import CDS
from aceofbases.sequence_methods import get_window_coords_within_cds
from aceofbases.sequence_methods import get_window_translations
from aceofbases.sequence_methods import get_padding_sequences
from aceofbases.sequence_methods import get_translations

from aceofbases.editing_sites import EditingSites
from aceofbases.base_editor import factory
from aceofbases.bed_interval import BedInterval

from argparse import ArgumentTypeError

class TestSequenceMethods(unittest.TestCase):
    
    def test_get_translations(self):
        tr = get_translations("TCACAGCATCATTCCTACCACGG")
        self.assertEqual(tr, ['SQHHSYH', 'HSIIPTT', 'TASFLPR', 'PW*E*CC', 'RGRNDAV', 'VVGMML*'])
    
    def test_get_window_coords_within_cds(self):
        base_editor = factory("BE4-Gam")
        candidates = EditingSites()
        candidates.add("AAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAANGG", 4, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("AAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAANGG", 26, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("AAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAANGG", 52, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("AAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAANGG", 75, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("AAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAANGG", 84, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("ACGTAGCCACGTACGTACGT", "ACGTAGCCACGTACGTACGTAGG", 42, "-", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("CGTAGGTACCTACGTACGTA", "CGTAGGTACCTACGTACGTACGG", 12, "-", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("GGTACCTACGTACGTACGGA", "GGTACCTACGTACGTACGGACGG", 8, "-", "fwdPrimer", "revPrimer",base_editor)
        cds = CDS("1",101,200,"+", "A"*71)
        cds.add_exon(101,130)
        cds.add_exon(160,200)

        window_coords=get_window_coords_within_cds(candidates.get_site("C1"),cds,base_editor)
        self.assertEqual(window_coords,[6,14])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C2"),cds,base_editor)
        self.assertEqual(window_coords,[28,30])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C3"),cds,base_editor)
        self.assertEqual(window_coords,[30,33])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C4"),cds,base_editor)
        self.assertEqual(window_coords,[48,56])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C5"),cds,base_editor)
        self.assertEqual(window_coords,[57,65])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C6"),cds,base_editor)
        self.assertEqual(window_coords,[30,34])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C7"),cds,base_editor)
        self.assertEqual(window_coords,[25,30])
        
        window_coords=get_window_coords_within_cds(candidates.get_site("C8"),cds,base_editor)
        self.assertEqual(window_coords,[21,29])
    
    def test_get_padding_sequences(self):
        base_editor = factory("BE4-Gam")
        # genomic position         0         1         2        25          7         8         9        9
        #                                    0         0        99          0         0         0        9
        # exon                                                  ||
        # cds index                0         0         0         0         0         0         0         0
        cds = CDS("1",101,200,"+","ACGTACGTCCGTCCGTACGTACGTAGGTACCTACGTACGTACGTGGCTACGTACGTACGTACGTACTGGCG")
        candidates = EditingSites()
        candidates.add("ACGTCCGTCCGTACGTACGT", "ACGTCCGTCCGTACGTACGTAGG", 4, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("TAGGTACCTACGTACGTACG", "TAGGTACCTACGTACGTACGTGG", 23, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("CTACGTACGTACGTACGTAC", "CTACGTACGTACGTACGTACTGG", 75, "+", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("ACGTAGCCACGTACGTACGT", "ACGTAGCCACGTACGTACGTAGG", 42, "-", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("NNNNNGTACCTACGTACGTA", "NNNNNGTACCTACGTACGTACGG", 12, "-", "fwdPrimer", "revPrimer",base_editor)
        candidates.add("NGTACCTACGTACGTACGGA", "NGTACCTACGTACGTACGGACGG", 8, "-", "fwdPrimer", "revPrimer",base_editor)
        cds.add_exon(101,130)
        cds.add_exon(160,200)

        pad = get_padding_sequences(cds,get_window_coords_within_cds(candidates.get_site("C1"),cds,base_editor))
        self.assertEqual(pad,["ACGTAC","GTACGTACG"])
        pad = get_padding_sequences(cds,get_window_coords_within_cds(candidates.get_site("C2"),cds,base_editor))
        self.assertEqual(pad,["TACGTACGTA","CTACGTACGTA"])
        pad = get_padding_sequences(cds,get_window_coords_within_cds(candidates.get_site("C3"),cds,base_editor))
        self.assertEqual(pad,["TACGTGGCT","ACGTACGTA"])
        pad = get_padding_sequences(cds,get_window_coords_within_cds(candidates.get_site("C4"),cds,base_editor))
        self.assertEqual(pad,["CGTAGGTAC","GTACGTACGT"])
        pad = get_padding_sequences(cds,get_window_coords_within_cds(candidates.get_site("C5"),cds,base_editor))
        self.assertEqual(pad,["TACGTACGTA","CTACGTACGTA"])
        pad = get_padding_sequences(cds,get_window_coords_within_cds(candidates.get_site("C6"),cds,base_editor))
        self.assertEqual(pad,["CCGTACGTA","CCTACGTAC"])
    
    def test_get_window_translations(self):
        # two exons from Olatipes rx2
        base_editor = factory("BE4-Gam")
        candidates = EditingSites()
        candidates.add("	GCATTTGTCAATGGATACCC", "GCATTTGTCAATGGATACCCTGG", 2, "+", "fwdP", "revP",base_editor) # The first target site
        candidates.add("	GGCGCTGGCTCCAGCAACGG", "GGCGCTGGCTCCAGCAACGGAGG", 78, "+", "fwdP", "revP",base_editor) # Fully inside of first coding exons
        candidates.add("	TCACAGCATCATTCCTACCA", "TCACAGCATCATTCCTACCACGG", 270, "+", "fwdP", "revP",base_editor) # The last G is outside of the CDS
        
        #CCTTCCGGAACTGGGGGACAGCT
        candidates.add("AGCTGTCCCCCAGTTCCGGA", "AGCTGTCCCCCAGTTCCGGAAGG",248,"-", "fwdP", "revP",base_editor)
        
        cds = CDS("17",30397660,30400780,"+", "ATGCATTTGTCAATGGATACCCTGGGGATGGTGGACGATAGCGGACATGGAATCGTTGACAGCTATGACATGGCGAAAGGCGCTGGCTCCAGCAACGGAGGCCGGGTCCACAGTATTGATGTCATATTGGGATTCACTAAAGACCAGGAGCCCTTGCTGCATTCAGTGGGAGATGATGGCAGCCCCAAAGTTAACGGAGCCCTCCAGCAGGACCCCGCCAAGCCGGTCCCTTCAGAACCCTATGGAAACCTTCCGGAACTGGGGGACAGCTCACAGCATCATTCCTACCACGACTCCAGTCTTTTCTCGAGTGATAAGTGTGAAGAAATGAGCAATCTCACCAAGGAAGTGGACATCGCTGACGGCTCTCCTAAGTCCATCAAAGAGGAAGAGCACGGCAAGAAAAAGCACAGAAGAAAC")
        cds.add_exon(30397660,30397951)
        cds.add_exon(30400653,30400780)
        
        window_translations = get_window_translations(candidates.get_site("C1"), cds, base_editor)
        self.assertEqual(window_translations.frames[0].translation, ['M', 'H', 'L', 'S>L', 'M', 'D', 'T'])
        self.assertEqual(window_translations.frames[0].window_start_aa, 1)
        self.assertEqual(window_translations.frames[0].window_end_aa, 4)
        self.assertEqual(window_translations.frames[1].translation, ['C', 'I', 'C', 'Q>*', 'W', 'I'])
        self.assertEqual(window_translations.frames[1].window_start_aa, 1)
        self.assertEqual(window_translations.frames[1].window_end_aa, 4)
        self.assertEqual(window_translations.frames[2].translation, ['A', 'F', 'V', 'N', 'G', 'Y'])
        self.assertEqual(window_translations.frames[2].window_start_aa, 0)
        self.assertEqual(window_translations.frames[2].window_end_aa, 4)
        self.assertEqual(window_translations.frames[3].translation, ['G', 'I', 'H', '*', 'Q', 'M', 'H'])
        self.assertEqual(window_translations.frames[3].window_start_aa, 3)
        self.assertEqual(window_translations.frames[3].window_end_aa, 6)
        self.assertEqual(window_translations.frames[4].translation, ['V', 'S', 'I', 'D>N', 'K', 'C'])
        self.assertEqual(window_translations.frames[4].window_start_aa, 2)
        self.assertEqual(window_translations.frames[4].window_end_aa, 6)
        
        window_translations = get_window_translations(candidates.get_site("C2"), cds, base_editor)
        self.assertEqual(window_translations.frames[0].translation, ['M', 'A', 'K', 'G', 'A>V', 'G', 'S', 'S', 'N', 'G'])
        self.assertEqual(window_translations.frames[0].window_start_aa, 3)
        self.assertEqual(window_translations.frames[0].window_end_aa, 7)
        
        # window_translations = get_window_translations(candidates.get_site("C3"), cds, base_editor)
        # self.assertEqual(window_translations.frames[0].translation, ['S', 'Y', 'H', 'G>D', 'S', 'S', 'L', 'F'])
        # self.assertEqual(window_translations.frames[0].window_start, 3)
        # self.assertEqual(window_translations.frames[0].window_end, 5)
        
        window_translations = get_window_translations(candidates.get_site("C4"), cds, base_editor)
        self.assertEqual(window_translations.frames[0].translation, ['P','E', 'L', 'G>K', 'D>N', 'S>N', 'S', 'Q', 'H'])
        self.assertEqual(window_translations.frames[0].window_start_aa, 3)
        self.assertEqual(window_translations.frames[0].window_end_aa, 6)
        
    def test_get_window_translations2(self):
        base_editor = factory("BE4-Gam")
        candidates = EditingSites()
        candidates.add("	GCATTTGTCAATGGATACCC", "GCATTTGTCAATGGATACCCTGG", 2, "+", "fwdP", "revP",base_editor)
        candidates.add("	CATTTGTCAATGGATACCCT", "CATTTGTCAATGGATACCCTGGG", 3, "+", "fwdP", "revP",base_editor)
        
        #CCCTGGGGATGGTGGACGATAGC
        candidates.add("GCTATCGTCCACCATCCCCA", "GCTATCGTCCACCATCCCCAGGG",19,"-", "fwdP", "revP",base_editor)
        
        cds = CDS("17",30397660,30397701,"+", "ATGCATTTGTCAATGGATACCCTGGGGATGGTGGACGATAGC")
        cds.add_exon(30397660,30397701)
        
        window_translations = get_window_translations(candidates.get_site("C1"), cds, base_editor)
        # print(window_translations.frames[0].translation,window_translations.frames[0].window_start_aa,window_translations.frames[0].window_end_aa,window_translations.frames[0].window_start_nt,window_translations.frames[0].window_end_nt)
        window_translations = get_window_translations(candidates.get_site("C2"), cds, base_editor)
        # print(window_translations.frames[0].translation,window_translations.frames[0].window_start_aa,window_translations.frames[0].window_end_aa,window_translations.frames[0].window_start_nt,window_translations.frames[0].window_end_nt)
        window_translations = get_window_translations(candidates.get_site("C3"), cds, base_editor)
        # print(window_translations.frames[0].translation,window_translations.frames[0].window_start_aa,window_translations.frames[0].window_end_aa,window_translations.frames[0].window_start_nt,window_translations.frames[0].window_end_nt)
        
    def test_get_window_translations3(self):
        base_editor = factory("BE4-Gam")
        candidates = EditingSites()

        candidates.add("TCATCGTCCTCCTTTGTCTG", "TCATCGTCCTCCTTTGTCTGTGG",93,"-", "fwdP", "revP",base_editor)
        candidates.add("CGTCCTCCTTTGTCTGTGGC", "CGTCCTCCTTTGTCTGTGGCTGG",96,"-", "fwdP", "revP",base_editor)
        
        cds = CDS("17",23216539,23216719,"+", "AGTCCTGATCCTGGGTCTCATCGAACTCTCATGGAACACCTATCCCTCAACAAACAGCAGCTCAGCCTCGCTCCACGTCTGTCACCTCATCGTCCTCCTTTGTCTGTGGCTGGCTCCACCTCTGCCCGCTGCCCCATCAGAAGCCCAGGAGCACACAGCTGATAAGGACAAACGCCACTGA")
        cds.add_exon(23216539,23216719)
        
        window_translations = get_window_translations(candidates.get_site("C1"), cds, base_editor)
        frame = 1
    
if __name__ == "__main__":
    unittest.main()


